# TG Reputation Bot

![Telegram Icon](http://icons.iconarchive.com/icons/froyoshark/enkel/128/Telegram-icon.png)

This bot gamifies a telegram group. Users can upvote and downvote messages,
gather reputation, climb up levels and win championships!

---

## How to install the bot

To use the bot, you have to download the code and install the needed
requirements for Python. Then run the main script. To always keep the bot
online, it is useful to let your computer/home server run the script as a service.

---

## Mechanics of the bot

- The bot will automatically scan the messages in a group for "+" or "-" as a
  reply to another message. If it has a positive, the bot will add 1 reputation
  point to the original poster of the message.

- To vote the user has to expend voting points. Per default users start with one
  vote per 24 hours. For each level and weekly championship the users gets one
  vote more for each 24 hours.

- Downvotes cost 3 votes, upvotes only one.

- Users cannot upvote themselves.

- Up- Downvotes have to be a reply to another message.

- Every Sunday the bot will pronounce a champion - the user with the most
  upvotes in the week. Users get a trophy icon for each won championship in
  their /myrep message and one upvote more.

- Users climb levels as they are upvotes. For each level a user has to collect n
  \*\* 2 upvotes (1 upvote for level 1, 4 upvotes for level 2, 9 upvotes for level
  3, 16 upvotes for level 4 and so on). One more vote is granted for each level.

### Commands

- /toprep shows the top 10 leaderboard of all time

- /weekly shows the weekly leaderboard

- /myrep shows the overall repuation of the user, their trophies, availiable
  votes and current level

- /help shows information for the user on the bot

- /rep shows a button menu for all the commands

---

## Requirements

- sqlalchemy
- python-telegram-bot

---

## Copyrights

Feel free to fork the code, use it as is or not. Would be nice to get creds, but
not mandatory.

### Improvements

Let me know feedback and improvement ideas via the issues. Or better yet, fully
written pull requests.
